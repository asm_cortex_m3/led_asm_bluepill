		.syntax		unified
		.cpu		cortex-m3
		.thumb

/****************************************/
/*	Defino constantes de programa		*/
/****************************************/
.equ PORTC_ODR,		0x4001100C		// .equ para definir constantes.
.equ GPIOC_CHR, 	0x40011004		// Puerto GPIOC
.equ RCC_APB2ENR, 	0x40021018		// Registros para habilitar el clock de GPIOC
.equ DEMORA, 		0xfffff			// Demora por software.

		.section	.text
		.align		2
 		.global		main

/****************************************/
/*	Función main. Acá salta boot.s 		*/
/*	cuando termina.						*/
/****************************************/
		.type		main, %function
main:
		BL		led_init		//Inicializo el pin como salida
		MOV		R0, #1			//Enciendo el led
		BL		led_set
main_loop:
		LDR		R0,=#DEMORA
		BL		delay
		BL		led_toggle
		B		main_loop		//Volver a empezar...


/****************************************/
/*	Función led_init. 				 	*/
/*	Inicializa El LED					*/
/****************************************/
		.type	led_init, %function
led_init:
		PUSH	{R1, R2, R3, LR}	// Mando a la pila los registros que modifico y LR
		LDR		R1, =(1 << 4)       // Cargo en R1 el bit que me habilita el GPIOC
		LDR 	R2, =#RCC_APB2ENR   // Cargo la dirección de memoria
		STR		R1, [R2]            // Habilito la señal de reloj para GPIOC
									// Pongo GPIOC13 como salida.

		LDR 	R2, =#GPIOC_CHR
		LDR		R3, [R2]
		BIC		R3, #(0x0F<<20)
		LDR 	R1, =#(0b11<< 20)
		ORR		R1, R3
		STR 	R1, [R2]
		POP		{R1, R2, PC}

/****************************************/
/*	Función led_set. 				 	*/
/*	Setea el led en funcion de R0		*/
/****************************************/
		.type	led_set, %function
led_set:
		PUSH	{R0, R1, R2, R3}	// Mando a la pila todos los registros que modifico
		MVN 	R0,R0				// R0   = ~R0
		MOV 	R1,#1				// R1   = 0x01
		AND 	R0,R0,R1			// R0 & = 0x01
		LSL 	R0,R0,#13			// R0 <<= 13
		LDR 	R2, =#PORTC_ODR   	// Escribo la dirección de memoria para setear GPIOC
		LDR		R3, [R2]			// Leo el registro ODR.
		BIC		R3, #(1<<13)		// Limpio el bit13.
		ORR		R0, R3				// Pongo el bit de la salida.
		STR 	R0, [R2]          	// Escribo el puerto de salida
		POP		{R0, R1, R2, R3}	// Repongo los registros que toqué.
		BX		LR					// return

/****************************************/
/*	Función led_toggle. 				*/
/*	Setea el led en funcion de R0		*/
/****************************************/
		.type	led_toggle, %function
led_toggle:
		PUSH	{R2, R3}			// Mando a la pila todos los registros que modifico
		LDR 	R2, =#PORTC_ODR   	// Escribo la dirección de memoria para setear GPIOC
		LDR		R3, [R2]			// Leo el registro ODR.
		EOR		R3, #(1<<13)		// XOR a 1 del bit 13 (lo invierto)
		STR 	R3, [R2]          	// Escribo el puerto de salida
		POP		{R2, R3}			// Repongo los registros que toqué.
		BX		LR					// return


/****************************************/
/*	Función delay. 				 		*/
/*	Recibe por R0 la demora				*/
/****************************************/
		.type	delay, %function
delay:
		PUSH	{R0, LR}			// Guardo el parámetro y LR en la pila.
delay_dec:
        SUBS	R0, 1				//
        BNE		delay_dec			// while(--R0);
		POP		{R0, PC}			// repongo R0 y vuelvo.
.end
